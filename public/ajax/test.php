<?
require($_SERVER["DOCUMENT_ROOT"]. "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader;
if (!CModule::IncludeModule("sale")) return;

$db_res1 = CSaleDiscount::GetByID(2);
$actions = unserialize($db_res1["ACTIONS"]);
$actions['CHILDREN'][0]['DATA']["Value"] = floor(10);
$db_res1["ACTIONS"] = $actions;
CSaleDiscount::Update(2,$db_res1);
?>
