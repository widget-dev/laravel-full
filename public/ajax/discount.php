<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
require_once($_SERVER["DOCUMENT_ROOT"]."/ajax/client.php");
if (CModule::IncludeModule('sale'))
{
	$date = new DateTime();
	$fields['COUPON'] = array(                        // массив $data
	  'DISCOUNT_ID' => 2,                        // id правила корзины
	  'ACTIVE_FROM' => \Bitrix\Main\Type\DateTime::createFromTimestamp($date->getTimestamp()),                        // выставляем без ограничения к началу даты активности купона
	  'ACTIVE_TO' => null,//\Bitrix\Main\Type\DateTime::createFromTimestamp(date_modify($date, date("H:i:s", mktime(0,$_GET["time"],0)))->getTimestamp()),                            // выставляем без ограничения к окончанию даты активности купона                  
	  'TYPE' => \Bitrix\Sale\Internals\DiscountCouponTable::TYPE_MULTI_ORDER, // выставляем тип купона TYPE_ONE_ORDER - использовать на один заказ, TYPE_MULTI_ORDER - использовать на несколько заказов 
	  'COUPON' => $_GET["code"],
	  'MAX_USE' => 1,                           // выставляем максимальное кол-во применений купона
	);
	$couponsResult = \Bitrix\Sale\Internals\DiscountCouponTable::add($fields['COUPON']);
	if (!$couponsResult->isSuccess())
	  $errors = $couponsResult->getErrorMessages();
}
use \Bitrix\Main\Loader;
if (!CModule::IncludeModule("sale")) return;
$db_res1 = CSaleDiscount::GetByID(2);
$actions = unserialize($db_res1["ACTIONS"]);
$actions['CHILDREN'][0]['DATA']["Value"] = floor($_GET["value"]);
$db_res1["ACTIONS"] = $actions;
CSaleDiscount::Update(2,$db_res1);
?>