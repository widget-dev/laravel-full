<?php

namespace App\Http\Controllers;

use App\Services\Widget\WidgetService;
use App\Services\Widget\ClientService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Coupon;
use Symfony\Component\HttpFoundation\Response;

/**
 * @property string $secretKey
 * @property string $token
 * @property integer $responseCode
 * @property string $url
 * @property string $code
 * @property string $userId
 * @property string $userIp
 *  */
class WidgetController extends Controller
{
    private const PROLONGATION = 'prolongation';
    private const USE = 'use';

    /**
     * @var widgetService
     */
    private $widgetService;

    /**
     * WidgetController constructor.
     * @param WidgetService $widgetService
     */
    public function __construct(WidgetService $widgetService){
        $this->widgetService = $widgetService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $widgetAction = null;
        if($request->request->has('act')){
            $widgetAction = $request->request->get('act');
        }

        if(empty(session('user_id'))){
            return new JsonResponse(
                'Session user_id is empty',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        if(empty(session('token'))){
            return new JsonResponse(
                'Session token is empty',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        $widgetDeals = new ClientService();
        $widgetDeals->setUserId(session('user_id'));
        $widgetDeals->setToken(session('token'));
        $widgetDeals->setUserIp($_SERVER['REMOTE_ADDR']);

        if ($widgetAction === self::PROLONGATION) {
            return $this->widgetService
                ->prolongation($widgetDeals, $request->request->get('code'));
        }

        if ($widgetAction === self::USE) {
            return $this->widgetService
                ->use($widgetDeals, $request->request->get('code'));
        }

        return $this->widgetService
            ->get($widgetDeals, $request->request->get('code'));
    }
}
